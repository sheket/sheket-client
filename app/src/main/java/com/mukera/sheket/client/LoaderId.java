package com.mukera.sheket.client;

/**
 * Created by gamma on 3/5/16.
 */
public interface LoaderId {
    int BRANCH_LIST_LOADER = -1;
    int BRANCH_ITEM_LIST_LOADER = -2;
    int ITEM_LIST_LOADER = 1;
    int ITEM_DETAIL_LOADER = 2;
    int SEARCH_RESULT_LOADER = 3;
    int COMPANY_LIST_LOADER = 4;
    int MEMBER_LIST_LOADER = 5;
    int TRANSACTION_HISTORY_LOADER = 6;
}
