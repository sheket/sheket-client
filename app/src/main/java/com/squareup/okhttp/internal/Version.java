package com.squareup.okhttp.internal;

/**
 * Created by gamma on 1/3/16.
 */
public class Version {
    public static String userAgent() {
        return "okhttp/2.1.0-RC1";
    }

    private Version() {

    }
}
